# Как развернуть проект
1. Склонируйте репозиторий
1. cd backend
1. python -m venv venv
1. source venv/bin/activate (venv\Scripts\activate для Windows)
1. pip install -r requirements.txt
1. python manage.py makemigrations
1. python manage.py migrate
1. python manage.py runserver
1. http://127.0.0.1:8000/admin/
1. Логин admin пароль admin

Если столкнётесь в проблемами в процессе пишите @GeoNinka
