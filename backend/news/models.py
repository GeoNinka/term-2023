from django.db import models

news_types = (
    ('news', 'NEWS'),
    ('event', 'EVENT'),
    ('announcement', 'ANNOUNCEMENT'),
)

class News(models.Model):
    title = models.CharField(max_length = 255)
    description = models.TextField()
    date = models.DateTimeField()
    news_type = models.CharField(max_length = 255, choices=news_types, default='news')
    image = models.ImageField(upload_to='images/')


    def __str__(self):
        return self.title

        