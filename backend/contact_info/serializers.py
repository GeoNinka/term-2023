from rest_framework import serializers

from .models import *


class ContactInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactInfo
        fields = ('id', 'phone', 'email')