from django.db import models

class Day(models.Model):
    day = models.CharField(max_length=255)
    start = models.TimeField()
    end = models.TimeField()
    
    def __str__(self):
        return self.day