from django.contrib import admin
from gallery.models import Gallery
from django.utils.safestring import mark_safe

class GalleryAdmin(admin.ModelAdmin):
    readonly_fields = ["preview"]

    def preview(self, obj):
        return mark_safe(f'<img src="{obj.image.url}" style="max-height: 300px;">')

admin.site.register(Gallery, GalleryAdmin)

