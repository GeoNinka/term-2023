from django.db import models
from activity.models import Activity 
from achievement.models import Achievement

class Trainer(models.Model):
    name = models.CharField(max_length = 255)
    surname = models.CharField(max_length = 255)
    middlename = models.CharField(max_length = 255)
    description = models.TextField()
    activity = models.ManyToManyField(Activity)
    price = models.IntegerField()
    achievements = models.ManyToManyField(Achievement)
    image = models.ImageField(upload_to='images/')


    def __str__(self):
        return self.name
