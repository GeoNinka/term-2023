from django.db import models

class Achievement(models.Model):
    title = models.CharField(max_length = 255)
    description = models.TextField()
    date = models.DateField()

    def __str__(self):
        return self.title